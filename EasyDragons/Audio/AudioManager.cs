using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Media;

namespace EasyDragons.Audio
{
	public static class AudioManager
	{
		#region Fields

		static float masterMusicVolume = 1f;
		static float masterSoundVolume = 1f;

		#endregion

		#region Properties

		public static float MasterMusicVolume
		{
			get { return masterMusicVolume; }
			set { masterMusicVolume = MathHelper.Clamp(value, 0, 1f); }
		}

		public static float MasterSoundVolume
		{
			get { return masterSoundVolume; }
			set { masterSoundVolume = MathHelper.Clamp(value, 0, 1f); }
		}

		#endregion

		#region Static Methods

		public static void PlayMusic(Music music)
		{
			MediaPlayer.Volume = MathHelper.Clamp(music.Volume, 0, masterMusicVolume);
			MediaPlayer.Play(music.Song);
			MediaPlayer.IsRepeating = music.Loop;
		}

		public static void StopMusic()
		{
			MediaPlayer.Stop();
		}

		public static void UpdateMusicVolume(float volume)
		{
			MediaPlayer.Volume = volume;
		}

		public static void PlaySound(Sound sound)
		{
			sound.SoundEffect.Volume = MathHelper.Clamp(sound.Volume, 0, masterSoundVolume);
			sound.SoundEffect.IsLooped = sound.Loop;

			if (!(sound.SoundEffect.State == Microsoft.Xna.Framework.Audio.SoundState.Playing))
			{
				sound.SoundEffect.Play();
			}
		}

		public static void StopSound(Sound sound)
		{
			sound.SoundEffect.Stop();
		}

		#endregion
	}
}
