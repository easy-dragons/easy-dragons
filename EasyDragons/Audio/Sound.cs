using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace EasyDragons.Audio
{
	public class Sound
	{
		#region Fields

		SoundEffectInstance sound;
		float volume;
		bool loop;

		#endregion

		#region Properties

		public SoundEffectInstance SoundEffect
		{
			get { return sound; }
		}

		public float Volume
		{
			get { return volume; }
			set { volume = value; }
		}

		public bool Loop
		{
			get { return loop; }
		}

		#endregion

		#region Constructor

		public Sound()
		{
		}

		#endregion

		#region Methods

		public void LoadContent(ContentManager content, string soundName, float volume = 1.0f, bool loop = false)
		{
			SoundEffect soundEffect = content.Load<SoundEffect>(soundName);
			sound = soundEffect.CreateInstance();
			this.volume = MathHelper.Clamp(volume, 0f, 1f);
			this.loop = loop;
		}

		#endregion
	}
}
