using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;

namespace EasyDragons.Audio
{
	public class Music
	{
		#region Fields

		Song song;
		float volume;
		bool loop;

		#endregion

		#region Properties

		public Song Song
		{
			get { return song; }
		}

		public float Volume
		{
			get { return volume; }
		}

		public bool Loop
		{
			get { return loop; }
		}

		#endregion

		#region Constructor

		public Music()
		{
		}

		#endregion

		#region Methods

		public void LoadContent(ContentManager content, string musicName, float volume = 1.0f, bool loop = true)
		{
			song = content.Load<Song>(musicName);
			this.volume = MathHelper.Clamp(volume, 0f, 1f);
			this.loop = loop;
		}

		#endregion
	}
}
