using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EasyDragons.Map
{
	public class TileMap
	{
		#region Fields

		int tileMapID;
		List<List<Tile>> layers;
		ContentManager content;

		#endregion

		#region Properties

		public int LayerCount
		{
			get { return layers.Count; }
		}

		#endregion

		#region Constructor

		public TileMap(int tileMapID, int layersAmount)
		{
			this.tileMapID = tileMapID;

			layers = new List<List<Tile>>();

			for (int i = 0; i < layersAmount; i++)
			{
				AddLayer();
			}
		}

		#endregion

		#region Methods

		public void AddLayer()
		{
			layers.Add(new List<Tile>());
		}

		public void RemoveLayer()
		{
			layers.RemoveAt(layers.Count - 1);
		}

		public void RemoveAllLayers()
		{
			layers.Clear();
		}

		public void ClearLayer(int layer)
		{
			if (layer <= layers.Count)
			{
				layers[layer - 1] = new List<Tile>();
			}
		}

		public void AddStaticTile(string tileName, Vector2 position, bool isPassable, int layer)
		{
			if ((layer >= 1) && (layer <= layers.Count))
			{
				if (GetTile(layer, position) == null)
				{
					StaticTile tile = new StaticTile(isPassable);
					tile.LoadContent(content, tileName);
					tile.Position = position;
					layers[layer - 1].Add(tile);
				}
			}
		}

		public void AddAnimatedTile(string tileName, Vector2 position, bool isPassable, int layer, int framesWidth, int framesHeight)
		{
			if ((layer >= 1) && (layer <= layers.Count))
			{
				if (GetTile(layer, position) == null)
				{
					AnimatedTile tile = new AnimatedTile(isPassable);
					tile.LoadContent(content, tileName, framesWidth, framesHeight);
					tile.Position = position;
					layers[layer - 1].Add(tile);
				}
			}
		}

		public void ChangeTile(int layer, Vector2 position, Tile newTile)
		{
			for (int i = 0; i < layers[layer - 1].Count; i++)
			{
				if (layers[layer - 1][i].Position == position)
				{
					newTile.Position = position;
					layers[layer - 1][i] = newTile;
				}
			}
		}

		public Tile GetTile(int layer, Vector2 position)
		{
			foreach (Tile tile in layers[layer - 1])
			{
				if (tile.Position == position)
				{
					return tile;
				}
			}

			return null;
		}

		public void LoadContent(ContentManager content)
		{
			this.content = content;
		}

		public void Update(GameTime gameTime)
		{
			foreach (List<Tile> layer in layers)
			{
				foreach (Tile tile in layer)
				{
					tile.Update(gameTime);
				}
			}
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			for (int i = 0; i < layers.Count; i++)
			{
				for (int j = 0; j < layers[i].Count; j++)
				{
					layers[i][j].Draw(spriteBatch);
				}
			}
		}

		#endregion
	}
}
