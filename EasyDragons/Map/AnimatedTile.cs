﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using EasyDragons.Animation;

namespace EasyDragons.Map
{
	public class AnimatedTile : Tile
	{
		#region Fields

		SpriteSheet texture;

		#endregion

		#region Properties

		public override Vector2 Position
		{
			get { return texture.Image.Position; }
			set { texture.Image.Position = value; }
		}

		public override Vector2 Size
		{
			get { return texture.Image.ImageSize; }
		}

		public override Rectangle SourceRectangle
		{
			get { return texture.Image.SourceRectangle; }
		}

		public int AnimationSpeed
		{
			get { return texture.AnimationSpeed; }
			set { texture.AnimationSpeed = MathHelper.Max(0, value); }
		}

		public bool IsAnimating
		{
			set { texture.IsAnimating = value; }
		}

		#endregion

		#region Constructor

		public AnimatedTile(bool isPassable)
			: base(isPassable)
		{
			texture = new SpriteSheet();
		}

		#endregion

		#region Methods

		public void LoadContent(ContentManager content, string tileName, int framesWidth, int framesHeight)
		{
			texture.LoadContent(content, tileName, framesWidth, framesHeight);
			texture.AddAnimation("", 0);
			texture.CurrentAnimation = "";
			texture.AnimationSpeed = 800;
			texture.IsAnimating = true;
			texture.Loop = true;
		}

		public override void Update(GameTime gameTime)
		{
			texture.Update(gameTime);
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			texture.Draw(spriteBatch);
		}

		#endregion
	}
}
