using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EasyDragons.Map
{
	public abstract class Tile
	{
		#region Fields

		bool isPassable;

		#endregion

		#region Properties

		public bool IsPassable
		{
			get { return isPassable; }
		}

		public abstract Vector2 Position { get; set; }

		public abstract Vector2 Size { get; }

		public abstract Rectangle SourceRectangle { get; }

		#endregion

		#region Constructor

		public Tile(bool isPassable = true)
		{
			this.isPassable = isPassable;
		}

		#endregion

		#region Abstract Methods

		public abstract void Update(GameTime gameTime);

		public abstract void Draw(SpriteBatch spriteBatch);

		#endregion
	}
}
