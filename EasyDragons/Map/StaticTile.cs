﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using EasyDragons.Animation;

namespace EasyDragons.Map
{
	public class StaticTile : Tile
	{
		#region Fields

		Image texture;

		#endregion

		#region Properties

		public override Vector2 Position
		{
			get { return texture.Position; }
			set { texture.Position = value; }
		}

		public override Vector2 Size
		{
			get { return texture.ImageSize; }
		}

		public override Rectangle SourceRectangle
		{
			get { return texture.SourceRectangle; }
		}

		#endregion

		#region Constructor

		public StaticTile(bool isPassable)
			: base(isPassable)
		{
			texture = new Image();
		}

		#endregion

		#region Methods

		public void LoadContent(ContentManager content, string tileName)
		{
			texture.LoadContent(content, tileName);
		}

		public override void Update(GameTime gameTime)
		{
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			texture.Draw(spriteBatch);
		}

		#endregion
	}
}
