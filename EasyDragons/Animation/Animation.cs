using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EasyDragons.Animation
{
	public abstract class Animation
	{
		#region Fields

		protected Image image;

		#endregion

		#region Abstract Methods

		public abstract void Update(GameTime gameTime);

		#endregion

		#region Virtual Methods

		public virtual void LoadContent(ContentManager content, string imageName)
		{
			image = new Image();
			image.LoadContent(content, imageName);
		}

		public virtual void Draw(SpriteBatch spriteBatch)
		{
			image.Draw(spriteBatch);
		}

		#endregion
	}
}
