using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EasyDragons.Animation
{
	public class Image
	{
		#region Fields

		Texture2D texture;
		Vector2 position;
		bool visible;
		Rectangle sourceRectangle;
		Color color;
		float rotation, layerDepth;
		Vector2 origin, scale;
		SpriteEffects spriteEffects;

		#endregion

		#region Properties

		public Texture2D Texture
		{
			get { return texture; }
		}

		public Vector2 Position
		{
			get { return position; }
			set { position = value; }
		}

		public bool Visible
		{
			get { return visible; }
			set { visible = value; }
		}

		public Rectangle SourceRectangle
		{
			get { return sourceRectangle; }
			set { sourceRectangle = value; }
		}

		public Color Color
		{
			get { return color; }
			set { color = value; }
		}

		public float Rotation
		{
			get { return rotation; }
			set { rotation = value; }
		}

		public float LayerDepth
		{
			get { return layerDepth; }
			set { rotation = MathHelper.Clamp(value, 0f, 1f); }
		}

		public Vector2 Origin
		{
			get { return origin; }
			set { origin = value; }
		}

		public Vector2 Scale
		{
			get { return scale; }
			set
			{
				scale.X = MathHelper.Max(0f, value.X);
				scale.Y = MathHelper.Max(0f, value.Y);
			}
		}

		public SpriteEffects SpriteEffects
		{
			get { return spriteEffects; }
			set { spriteEffects = value; }
		}

		public Vector2 ImageSize
		{
			get { return new Vector2(texture.Width, texture.Height); }
		}

		#endregion

		#region Constructor

		public Image()
		{
			position = Vector2.Zero;
			visible = true;
			color = Color.White;
			rotation = 0;
			layerDepth = 0;
			scale = Vector2.One;
			spriteEffects = SpriteEffects.None;
		}

		#endregion

		#region Methods

		public void LoadContent(ContentManager content, string name)
		{
			texture = content.Load<Texture2D>(name);
			sourceRectangle = texture.Bounds;
			origin = new Vector2(texture.Width / 2, texture.Height / 2);
		}

		public void Draw(SpriteBatch spriteBatch)
		{
			if (visible)
			{
				spriteBatch.Draw(texture, position, sourceRectangle, color, rotation, origin, scale, spriteEffects, layerDepth);
			}
		}
	}

	#endregion
}
