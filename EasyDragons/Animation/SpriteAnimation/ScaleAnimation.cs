﻿using System;

using Microsoft.Xna.Framework;

namespace EasyDragons.Animation.SpriteAnimation
{
	public class ScaleAnimation : SpriteAnimation
	{
		#region Fields

		float lowerLimit, upperLimit, scaleSpeed, scaleTimer;
		Vector2 originalScale, newScale;
		int loopCounter;

		#endregion

		#region Properties

		public float ScaleSpeed
		{
			get { return scaleSpeed; }
			set { scaleSpeed = MathHelper.Clamp(value, 0.001f, upperLimit); }
		}

		Vector2 scale
		{
			get { return newScale; }

			set
			{
				newScale.X = MathHelper.Clamp(MathHelper.Max(0, value.X), lowerLimit, upperLimit);
				newScale.Y = MathHelper.Clamp(MathHelper.Max(0, value.Y), lowerLimit, upperLimit);
			}
		}

		#endregion

		#region Constructor

		public ScaleAnimation(float lowerLimit = 1f, float upperLimit = 2f, float scaleSpeed = 0.08f, bool loop = false)
		{
			Enabled = false;
			Loop = loop;
			originalScale = Vector2.Zero;
			this.lowerLimit = MathHelper.Max(0, lowerLimit);
			this.upperLimit = MathHelper.Max(lowerLimit, upperLimit);
			ScaleSpeed = scaleSpeed;
			scaleTimer = 0f;
			loopCounter = 0;
		}

		#endregion

		#region Methods

		public override void Update(GameTime gameTime, Image image)
		{
			if (originalScale == Vector2.Zero)
			{
				originalScale = image.Scale;
			}

			if (Enabled)
			{
				scaleTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

				if (scaleTimer > 100)
				{
					scaleTimer = 0;

					scale = image.Scale;

					if (scale.X == upperLimit || scale.Y == upperLimit || scale.X == lowerLimit || scale.Y == lowerLimit)
					{
						scaleSpeed *= -1;

						if (!Loop)
						{
							loopCounter++;
						}
					}

					scale = new Vector2(scale.X + ScaleSpeed, scale.Y + scaleSpeed);
					image.Scale = scale;

					if (!Loop)
					{
						if (loopCounter >= 2 && (scale.X == lowerLimit || scale.X <= originalScale.X || scale.Y == lowerLimit || scale.Y <= originalScale.Y))
						{
							Enabled = false;
							loopCounter = 0;
						}
					}
				}
			}
			else
			{
				image.Scale = originalScale;

				if (scaleSpeed < 0)
				{
					scaleSpeed *= -1;
				}
			}
		}

		#endregion
	}
}
