﻿
using Microsoft.Xna.Framework;

namespace EasyDragons.Animation.SpriteAnimation
{
	public class ColorSwapAnimation : SpriteAnimation
	{
		#region Fields

		Color oldColor, newColor;
		bool colorChanged;

		#endregion

		#region Properties

		public Color OldColor
		{
			get { return oldColor; }
			set { oldColor = value; }
		}

		public Color NewColor
		{
			get { return newColor; }
			set { newColor = value; }
		}

		#endregion

		#region Constructor

		public ColorSwapAnimation(Color oldColor, Color newColor)
		{
			this.oldColor = oldColor;
			this.newColor = newColor;
			colorChanged = false;
		}

		#endregion

		#region Methods

		void changeColor(Image image, Color oldColor, Color newColor)
		{
			Color[] imageColors = new Color[(int)image.ImageSize.X * (int)image.ImageSize.Y];

			image.Texture.GetData<Color>(imageColors);

			for (int i = 0; i < imageColors.Length; i++)
			{
				if (imageColors[i] == oldColor)
				{
					imageColors[i] = newColor;
				}
			}

			image.Texture.SetData<Color>(imageColors);
		}

		public override void Update(GameTime gameTime, Image image)
		{
			if (Enabled)
			{
				if (!colorChanged)
				{
					changeColor(image, oldColor, newColor);
					colorChanged = true;
				}
			}
			else
			{
				if (colorChanged)
				{
					changeColor(image, newColor, oldColor);
					colorChanged = false;
				}
			}
		}

		#endregion
	}
}
