﻿using Microsoft.Xna.Framework;

namespace EasyDragons.Animation.SpriteAnimation
{
	public class OutlineAnimation : SpriteAnimation
	{
		#region Fields

		float[] textureMask, outlineMask;
		Color outlineColor;
		int outlineThickness;

		#endregion

		#region Properties

		public Color OutlineColor
		{
			get { return outlineColor; }
			set { outlineColor = value; }
		}

		public int OutlineThickness
		{
			get { return outlineThickness; }
			set { outlineThickness = MathHelper.Max(1, value); }
		}

		#endregion

		#region Constructor

		public OutlineAnimation(Color outlineColor, int outlineThickness = 3)
		{
			this.outlineColor = outlineColor;
			OutlineThickness = outlineThickness;
		}

		#endregion

		#region Methods

		void createTextureMask(Image image)
		{
			int textureSize = (int)image.ImageSize.X * (int)image.ImageSize.Y;

			textureMask = new float[textureSize];

			Color[] imageColors = new Color[textureSize];

			image.Texture.GetData<Color>(imageColors);

			for (int i = 0; i < imageColors.Length; i++)
			{
				if (imageColors[i].A != 0)
				{
					textureMask[i] = 1;
				}
				else
				{
					textureMask[i] = 0;
				}
			}
		}

		void createOutlineMask(Image image)
		{
			int height = (int)image.ImageSize.Y;
			int width = (int)image.ImageSize.X;
			int size = height * width;

			outlineMask = new float[size];

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++)
				{
					int t = 1;

					int center = x + y * width;

					if (textureMask[center] == 1)
					{
						outlineMask[center] = 1;
						continue;
					}

					while (t <= outlineThickness)
					{
						int up = center - (width * t);
						int down = center + (width * t);
						int left = center - t;
						int right = center + t;

						if ((up >= 0) && (textureMask[up] == 1) ||
							((down < textureMask.Length) && (textureMask[down] == 1)) ||
							((left >= y * width) && (textureMask[left] == 1)) ||
							((right < (y + 1) * width) && (textureMask[right] == 1)))
						{
							outlineMask[center] = 1;
							break;
						}

						t++;
					}
				}
			}
		}

		void applyOutline(Image image)
		{
			Color[] imageColors = new Color[(int)image.ImageSize.X * (int)image.ImageSize.Y];

			image.Texture.GetData<Color>(imageColors);

			for (int i = 0; i < imageColors.Length; i++)
			{
				if (imageColors[i].A == 0 && outlineMask[i] != 0)
				{
					imageColors[i] = outlineColor * outlineMask[i];
				}
			}

			image.Texture.SetData<Color>(imageColors);
		}

		void disableOutline(Image image)
		{
			Color[] imageColors = new Color[(int)image.ImageSize.X * (int)image.ImageSize.Y];

			image.Texture.GetData<Color>(imageColors);

			for (int i = 0; i < imageColors.Length; i++)
			{
				imageColors[i] *= textureMask[i];
			}

			image.Texture.SetData<Color>(imageColors);
		}

		public override void Update(GameTime gameTime, Image image)
		{
			if (Enabled)
			{
				if (textureMask == null)
				{
					createTextureMask(image);
					createOutlineMask(image);
					applyOutline(image);
				}
			}
			else
			{
				if (textureMask != null)
				{
					disableOutline(image);
					textureMask = null;
					outlineMask = null;
				}
			}
		}

		#endregion
	}
}
