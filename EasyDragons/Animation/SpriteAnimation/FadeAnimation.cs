﻿using System;

using Microsoft.Xna.Framework;

namespace EasyDragons.Animation.SpriteAnimation
{
	public class FadeAnimation : SpriteAnimation
	{
		#region Fields

		float fadeSpeed, fadeTimer;

		#endregion

		#region Properties

		public float FadeSpeed
		{
			get { return fadeSpeed; }
			set { fadeSpeed = MathHelper.Clamp(value, 0.01f, 1); }
		}

		#endregion

		#region Constructor

		public FadeAnimation(float fadeSpeed = 0.10f, bool loop = false)
		{
			Enabled = false;
			Loop = loop;
			FadeSpeed = fadeSpeed;
			fadeTimer = 0f;
		}

		#endregion

		#region Methods

		public Color FadeColor(Color color)
		{
			if (color.A == 255 || color.A == 0)
			{
				fadeSpeed *= -1;
			}

			int fadeAmount = (int)(255 * fadeSpeed);

			int r = color.R + fadeAmount;
			int g = color.G + fadeAmount;
			int b = color.B + fadeAmount;
			int a = color.A + fadeAmount;

			return new Color(r, g, b, a);
		}

		public override void Update(GameTime gameTime, Image image)
		{
			if (Enabled)
			{
				fadeTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

				if (fadeTimer > 100)
				{
					fadeTimer = 0;

					image.Color = FadeColor(image.Color);

					if (!Loop && image.Color == Color.White)
					{
						Enabled = false;
					}
				}
			}
			else
			{
				image.Color = Color.White;

				if (fadeSpeed < 0)
				{
					fadeSpeed *= -1;
				}
			}
		}

		#endregion
	}
}
