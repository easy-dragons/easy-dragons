﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EasyDragons.Animation.SpriteAnimation
{
	public class ColorFadeAnimation : SpriteAnimation
	{
		#region Fields

		Color[] originalColors;
		Color newColor;
		float colorFadeSpeed, colorFadeTimer, colorFadeAmount;
		int loopCounter;

		#endregion

		#region Properties

		public float ColorFadeSpeed
		{
			get { return colorFadeSpeed; }
			set { colorFadeSpeed = MathHelper.Clamp(value, 0.01f, 1); }
		}

		public Color Color
		{
			get { return newColor; }
			set { newColor = value; }
		}

		float amount
		{
			get { return colorFadeAmount; }
			set { colorFadeAmount = MathHelper.Clamp(value, 0f, 1f); }
		}

		#endregion

		#region Constructor

		public ColorFadeAnimation(Color newColor, float colorFadeSpeed = 0.1f, bool loop = false)
		{
			Enabled = false;
			Loop = loop;
			this.newColor = newColor;
			originalColors = null;
			this.colorFadeSpeed = colorFadeSpeed;
			colorFadeAmount = 0f;
			loopCounter = 0;
		}

		#endregion

		#region Methods

		public override void Update(GameTime gameTime, Image image)
		{
			if (Enabled)
			{
				if (originalColors == null)
				{
					originalColors = new Color[(int)image.ImageSize.X * (int)image.ImageSize.Y];
					image.Texture.GetData<Color>(originalColors);
				}

				colorFadeTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

				if (colorFadeTimer > 100)
				{
					colorFadeTimer = 0;

					amount += colorFadeSpeed;

					if (amount == 0f || amount == 1f)
					{
						colorFadeSpeed *= -1;

						if (!Loop)
						{
							loopCounter++;
						}
					}

					Color[] imageColors = new Color[(int)image.ImageSize.X * (int)image.ImageSize.Y];
					image.Texture.GetData<Color>(imageColors);

					for (int i = 0; i < imageColors.Length; i++)
					{
						if (imageColors[i].A != 0f)
						{
							imageColors[i] = Color.Lerp(originalColors[i], newColor, amount);
						}
					}

					image.Texture.SetData<Color>(imageColors);

					if (!Loop)
					{
						if (loopCounter == 2)
						{
							Enabled = false;
							loopCounter = 0;
						}
					}
				}
			}
			else
			{
				if (originalColors != null)
				{
					image.Texture.SetData<Color>(originalColors);
					originalColors = null;
				}

				amount = 0f;

				if (colorFadeSpeed < 0)
				{
					colorFadeSpeed *= -1;
				}
			}
		}

		#endregion
	}
}
