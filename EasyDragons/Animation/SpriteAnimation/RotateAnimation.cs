﻿using Microsoft.Xna.Framework;

namespace EasyDragons.Animation.SpriteAnimation
{
	public class RotateAnimation : SpriteAnimation
	{
		#region Fields

		float rotateSpeed, rotateAmount, rotateTimer;

		#endregion

		#region Properties

		public float RotateSpeed
		{
			get { return rotateSpeed; }
			set { rotateSpeed = MathHelper.Clamp(value, -100f, 100); }
		}

		#endregion

		#region Constructor

		public RotateAnimation(float rotateSpeed = 0.2f, bool loop = false)
		{
			Enabled = false;
			Loop = loop;
			RotateSpeed = rotateSpeed;
			rotateTimer = 0f;
			rotateAmount = 0;
		}

		#endregion

		#region Methods

		public override void Update(GameTime gameTime, Image image)
		{
			if (Enabled)
			{
				rotateTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

				if (rotateTimer > 10)
				{
					rotateTimer = 0;

					rotateAmount += rotateSpeed;

					float degrees = MathHelper.ToDegrees(image.Rotation);
					degrees += rotateSpeed;
					degrees = degrees % 360;

					image.Rotation = MathHelper.ToRadians(degrees);

					if (!Loop)
					{
						if (rotateAmount <= -360)
						{
							Enabled = false;
							image.Rotation -= MathHelper.ToRadians(rotateAmount + 360);
						}
						else if (rotateAmount >= 360)
						{
							Enabled = false;
							image.Rotation -= MathHelper.ToRadians(rotateAmount - 360);
						}
					}
				}
			}
			else
			{
				rotateAmount = 0f;
			}
		}

		#endregion
	}
}
