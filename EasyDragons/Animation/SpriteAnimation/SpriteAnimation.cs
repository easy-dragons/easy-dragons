﻿using System;

using Microsoft.Xna.Framework;

namespace EasyDragons.Animation.SpriteAnimation
{
	public abstract class SpriteAnimation
	{
		#region Fields

		bool enabled, loop;

		#endregion

		#region Properties

		public bool Enabled
		{
			get { return enabled; }
			set { enabled = value; }
		}

		public bool Loop
		{
			get { return loop; }
			set { loop = value; }
		}

		#endregion

		#region Abstract Methods

		public abstract void Update(GameTime gameTime, Image image);

		#endregion
	}
}
