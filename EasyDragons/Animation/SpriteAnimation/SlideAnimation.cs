﻿using System;

using Microsoft.Xna.Framework;

namespace EasyDragons.Animation.SpriteAnimation
{
	public class SlideAnimation : SpriteAnimation
	{
		#region Fields

		float slideSpeed, defaultSlideSpeed, slideAmount, slideTimer;
		int distance, loopCounter;
		bool leftToRight;

		#endregion

		#region Properties

		public float SlideSpeed
		{
			get { return defaultSlideSpeed; }
			set { defaultSlideSpeed = MathHelper.Clamp(value, -1f, 1); }
		}

		public int Distance
		{
			get { return distance; }
			set { distance = MathHelper.Max(0, value); }
		}

		#endregion

		#region Constructor

		public SlideAnimation(int distance = 10, float slideSpeed = 1f, bool leftToRight = true, bool loop = false)
		{
			Enabled = false;
			Loop = loop;
			Distance = distance;
			SlideSpeed = slideSpeed;
			this.slideSpeed = slideTimer = slideAmount = 0f;
			loopCounter = 0;
			this.leftToRight = leftToRight;
		}

		#endregion

		#region Methods

		public override void Update(GameTime gameTime, Image image)
		{
			if (Enabled)
			{
				if (slideSpeed == 0f)
				{
					slideSpeed = SlideSpeed;
				}

				slideTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

				if (slideTimer > 10)
				{
					slideTimer = 0;

					slideAmount = MathHelper.Clamp(slideAmount + slideSpeed, -distance, distance);

					if (slideAmount == -distance || slideAmount == distance)
					{
						slideSpeed *= -1;

						if (!Loop)
						{
							loopCounter++;
						}
					}

					if (leftToRight)
					{
						image.Position = new Vector2(image.Position.X + slideSpeed, image.Position.Y);
					}
					else
					{
						image.Position = new Vector2(image.Position.X, image.Position.Y + slideSpeed);
					}

					if (!Loop)
					{
						if (loopCounter >= 2 && Math.Abs(slideAmount - distance / 2) <= distance / 2)
						{
							Enabled = false;
							loopCounter = 0;
						}
					}
				}
			}
			else
			{
				slideSpeed = 0f;
				slideAmount = 0f;
			}
		}

		#endregion
	}
}
