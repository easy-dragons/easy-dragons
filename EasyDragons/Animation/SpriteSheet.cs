using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EasyDragons.Animation
{
	public class SpriteSheet : Animation
	{
		#region Fields

		Dictionary<string, float> animations;
		Vector2 frameSize, currentFrame;
		int framesWidth, framesHeight, animationSpeed;
		float timer;
		string currentAnimation;
		bool isAnimating, loop;

		#endregion

		#region Properties

		public Vector2 CurrentFrame
		{
			get { return currentFrame; }
			set
			{
				if (value.X >= framesWidth)
				{
					currentFrame.X = 0;
				}
				else if (value.Y >= framesHeight)
				{
					currentFrame.Y = 0;
				}
				else
				{
					currentFrame = value;
				}
			}
		}

		public string CurrentAnimation
		{
			set { currentAnimation = value; }
		}

		public bool IsAnimating
		{
			set { isAnimating = value; }
		}

		public Vector2 FrameSize
		{
			get { return frameSize; }
		}

		public Image Image
		{
			get { return image; }
			set { image = value; }
		}

		public int AnimationSpeed
		{
			get { return animationSpeed; }
			set { animationSpeed = value; }
		}

		public bool Loop
		{
			set { loop = value; }
		}

		#endregion

		#region Constructor

		public SpriteSheet()
		{
			animations = new Dictionary<string, float>();
			currentFrame = new Vector2(0, 0);
			isAnimating = loop = false;
			animationSpeed = 200;
		}

		#endregion

		#region Methods

		public void AddAnimation(string animationName, int animationIndex)
		{
			animations.Add(animationName, animationIndex);
		}

		public void LoadContent(ContentManager content, string imageName, int framesWidth, int framesHeight)
		{
			base.LoadContent(content, imageName);

			this.framesWidth = framesWidth;
			this.framesHeight = framesHeight;

			frameSize.X = (int)(image.ImageSize.X / framesWidth);
			frameSize.Y = (int)(image.ImageSize.Y / framesHeight);

			currentFrame = new Vector2(0, 0);

			image.Origin = new Vector2(frameSize.X / 2, frameSize.Y / 2);
		}

		public override void Update(GameTime gameTime)
		{
			if (animations.Count > 0)
			{
				if (isAnimating)
				{
					timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

					if (timer > animationSpeed)
					{
						timer = 0;

						currentFrame.Y = animations[currentAnimation];
						CurrentFrame = new Vector2(CurrentFrame.X + 1, CurrentFrame.Y);
					}
				}

				if (!loop)
				{
					isAnimating = false;
				}
			}
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			image.SourceRectangle = new Rectangle((int)(CurrentFrame.X * frameSize.X), (int)(CurrentFrame.Y * frameSize.Y), (int)frameSize.X, (int)frameSize.Y);
			image.Draw(spriteBatch);
		}

		#endregion
	}
}
