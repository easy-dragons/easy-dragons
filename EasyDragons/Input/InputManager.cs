using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework.Input;

namespace EasyDragons.Input
{
	public enum Key
	{
		Up,
		Down,
		Left,
		Right,
		Accept,
		Cancel,
		Start,
	}

	public class InputManager
	{
		#region Fields

		Dictionary<Key, Keys> controls;
		Keys[] defaultKeys;
		KeyboardState previousState, currentState;

		#endregion

		#region Constructor

		public InputManager()
		{
			controls = new Dictionary<Key, Keys>();

			if (defaultKeys == null)
			{
				defaultKeys = new Keys[]
				{
					Keys.Up,
					Keys.Down,
					Keys.Left,
					Keys.Right,
					Keys.Z,
					Keys.X,
					Keys.Enter
				};
			}

			ResetDefault();

			currentState = Keyboard.GetState();
			previousState = Keyboard.GetState();
		}

		public InputManager(Keys[] defaultKeys)
			: this()
		{
			if (Enum.GetNames(typeof(Key)).Length == defaultKeys.Length)
			{
				this.defaultKeys = defaultKeys;
				ResetDefault();
			}
		}

		#endregion

		#region Methods

		void assignKeys(Keys[] keys)
		{
			if (Enum.GetNames(typeof(Key)).Length == keys.Length)
			{
				controls.Clear();

				string[] keyString = Enum.GetNames(typeof(Key));

				for (int i = 0; i < keyString.Length; i++)
				{
					Key tempKey;
					Enum.TryParse(keyString[i], out tempKey);
					controls.Add(tempKey, keys[i]);
				}
			}
		}

		public void ResetDefault()
		{
			assignKeys(defaultKeys);
		}

		public void ChangeKey(Key key, Keys keys)
		{
			controls.Remove(key);
			controls.Add(key, keys);
		}

		public void ChangeKeys(Keys[] keys)
		{
			assignKeys(keys);
		}

		public bool KeyPressed(Key key)
		{
			return currentState.IsKeyDown(controls[key]);
		}

		public bool KeyReleased(Key key)
		{
			return currentState.IsKeyUp(controls[key]) && previousState.IsKeyDown(controls[key]);
		}

		public void Update()
		{
			previousState = currentState;
			currentState = Keyboard.GetState();
		}

		#endregion
	}
}
