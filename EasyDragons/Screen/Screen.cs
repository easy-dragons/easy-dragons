using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace EasyDragons.Screen
{
	public abstract class Screen
	{
		#region Fields

		string screenID;

		#endregion

		#region Properties

		public string ScreenID
		{
			get { return screenID; }
		}

		#endregion

		#region Constructor

		public Screen(string screenID)
		{
			this.screenID = screenID;
		}

		#endregion

		#region Abstract Methods

		public abstract void OnExit();

		public abstract void LoadContent(ContentManager content);

		public abstract void Update(GameTime gameTime);

		public abstract void Draw(SpriteBatch spriteBatch);

		#endregion
	}
}
