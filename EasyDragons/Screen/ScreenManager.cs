using System.Collections.Generic;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace EasyDragons.Screen
{
	public static class ScreenManager
	{
		#region Fields

		static Stack<Screen> screens = new Stack<Screen>();

		#endregion

		#region Properties

		public static int ScreenCount
		{
			get { return screens.Count; }
		}

		#endregion

		#region Static Methods

		public static void AddScreen(Screen screen)
		{
			screens.Push(screen);
		}

		public static void RemoveScreen()
		{
			if (ScreenCount > 0)
			{
				screens.Peek().OnExit();
				screens.Pop();
			}
		}

		public static void RemoveScreenUntil(string screenID)
		{
			while ((ScreenCount > 0) && (screens.Peek().ScreenID != screenID))
			{
				screens.Peek().OnExit();
				screens.Pop();
			}
		}

		public static void Update(GameTime gameTime)
		{
			if (ScreenCount > 0)
			{
				screens.Peek().Update(gameTime);
			}
		}

		public static void Draw(SpriteBatch spriteBatch)
		{
			if (ScreenCount > 0)
			{
				screens.Peek().Draw(spriteBatch);
			}
		}

		#endregion
	}
}
