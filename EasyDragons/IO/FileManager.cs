using System.Collections.Generic;
using System.IO;

namespace EasyDragons.IO
{
	public interface IFileManager
	{
		bool FileExists(string path);
		void CreateDirectory(string path);
	}

	public static class FileManager
	{
		#region Static Methods

		public static void WriteCSV(StreamWriter streamWriter, object[][] data, char delimiter = ';')
		{
			for (int i = 0; i < data.Length; i++)
			{
				for (int j = 0; j < data[i].Length; j++)
				{
					if (data[i][j] != null)
					{
						streamWriter.Write(data[i][j]);
						streamWriter.Write(delimiter);
					}
				}

				if (i < data.GetLength(0) - 1)
				{
					streamWriter.Write('\n');
				}
			}

			streamWriter.Dispose();
		}

		public static string[][] ReadCSV(StreamReader streamReader, char delimiter = ';')
		{
			string data = "";

			while (!streamReader.EndOfStream)
			{
				data += streamReader.ReadLine();
				data += '\n';
			}

			streamReader.Dispose();

			List<string[]> newData = new List<string[]>();
			string[] temp = data.Split('\n');

			for (int i = 0; i < temp.Length - 1; i++)
			{
				newData.Add(temp[i].Remove(temp[i].Length - 1).Split(delimiter));
			}

			return newData.ToArray();
		}

		#endregion
	}
}
