# Easy Dragons

Open source RPG engine created in C# and Monogame as a PCL.

## Overview

The idea is to create a simple RPG engine to cover the basics of RPGs focusing on Turn-RPGs. With the original Final Fantasy in mind, the objective is to provide a simple way to start your RPG game and from then, add new features.

## FAQ

### Can I contribute?

Yes, please! Read the contributing guidelines __CONTRIBUTING.md__ first.

### How I contribute?

There are several ways for you to contribute:
* Posting new issues about bugs or enhancements
* Adding new functionalities to the engine
* Creating an open source game using this engine

### Why a PCL?

The answer is simple, Monodevelop doesn't support autocompletion for shared projects.