# Contributing

## Git Guidelines

* Use present tense and imperative mood for commits or pull requests (examples: "Add Screen class", "Fix typo", ...)
* Use present tense for issues, be descriptive and state if it's an enhancement or a bug (examples: "Window bug", "Character error", "New job system support", ...)
* Use _kebab-case_ and meaningful names for branches according to the feature that it implements (examples: "issue-10", "windows-support", "sprites-manager", ...)

## Code Guidelines

#### Use PascalCase:
* Namespaces
* (+ prefix I) Interfaces (examples: "IGameComponent")
* Classes
* Enums
* Structs
* Properties
* Public methods or fields

#### Use camelCase:
* Function arguments
* Private and protected methods or fields

#### Naming conventions:
* Use concepts for namespaces (examples: "Animation", "IO", "GameLogic", ...)
* Use nouns or noun phrases for class names (examples: "InputManager", "Screen", "Inventory", ...)
* Use verbs or verb phrases for method names (examples: "OpenFile", "AddClass", "FillInventory", ...)
* Use nouns, noun phrases, or abbreviations of nouns for field names (examples: "color", "gameFont", "timer", ...)

#### Programming conventions:
* Keep _LoadContent_, _Update_ and _Draw_ methods when needed
* Function arguments are named after the field it interects with

#### Code Readability:
* Use regions in each class, keep the order of them and their respective code within them and only use these regions: _Constant/Static Fields_, _Fields_, _Properties_, _Constructor_, _[Abstract][Virtual][Static] Methods_ and _Methods_
* Use brackets in a new line only except for properties and switch cases (not including break) of one line
* Use line breaks to separate each enum values or in multi-dimensional array assignaments
* Use the same declaration for multiple variables (example: "int a, b, c;")*
* Prefer to name the variables as their types when the type isn't provided by C# (examples: "SpriteBatch spriteBatch", "Screen screen", ...)

#### Dont's:
* Don't use public fields except for singletons
* Don't use private modifier for private fields or methods
* Don't use prefixes (except for Interfaces) or underscores
* Don't comment everything. If you need several comments, then the code is not readable enough
* Don't leave old code in comments

### Good Code Example:

```csharp

public interface IComponent
{
    method(Field field);
}

public enum E
{
    One,
    Two,
    Three,
}

public class C : IComponent
{
    Field field;

    Array array = new array[x, y]
    {
        {
            //multiples lines
        }

        {
            //multiples lines
        }
    }

    property P
    {
        get{ }  //single line

        set
        {
            //multiples lines 
        }
    }

    method(Field field)
    {
        this.field = field;

        switch(field)
        {
           case 1: { break; }  //single line

           case 2: 
             {
                  //multiples lines
                  break;
             } 
        }

        if(something)
        {
             //single line
        }
    }
}
```

